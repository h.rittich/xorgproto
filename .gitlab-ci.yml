# vim: set expandtab shiftwidth=2 tabstop=8 textwidth=0:

include:
  # Arch container builder template
  - project: 'freedesktop/ci-templates'
    ref: 6f86b8bcb0cd5168c32779c4fea9a893c4a0c046
    file:
      - '/templates/arch.yml'

# global variables to be used by most/all jobs.
variables:
  FDO_UPSTREAM_REPO: 'xorg/proto/xorgproto'
  # Changing the tag will rebuild the container images. The value is just a
  # string, but we use the date for human benefits.
  FDO_DISTRIBUTION_TAG: '2021-02-24.0'

stages:
  - prep
  - check
  - build
  - test

container-prep:
  extends:
    - .fdo.container-build@arch
  stage: prep
  variables:
    GIT_STRATEGY: none
    # minimal set of packages required to build and install
    BASE_PACKAGES: 'meson ninja gcc autoconf automake make xorg-util-macros pkgconf'
    # extra packages we need for various tests
    EXTRA_PACKAGES: 'git libevdev python python-libevdev python-black'
    FDO_DISTRIBUTION_PACKAGES: $BASE_PACKAGES $EXTRA_PACKAGES

meson:
  extends:
    - .fdo.distribution-image@arch
  stage: build
  parallel:
    matrix:
      - MESON_OPTIONS: ['', '-Dlegacy=true']
  script:
    - mkdir -p ../_inst
    - meson builddir --prefix="$PWD/../_inst" $MESON_OPTIONS
    - meson configure builddir
    - ninja -C builddir test
    - ninja -C builddir install

autotools:
  extends:
    - .fdo.distribution-image@arch
  stage: build
  parallel:
    matrix:
      - CONFIGURE_OPTIONS: ['', '--enable-legacy']
  script:
    - mkdir -p ../_inst _build
    - autoreconf -ivf
    - pushd _build
    - ../configure --prefix="$PWD/../_inst" $CONFIGURE_OPTIONS
    - make install
    - make distcheck
    - mv xorgproto*.tar.gz ..
    - popd
  artifacts:
    paths:
      - xorgproto*.tar.gz

meson from tarball:
  extends:
    - .fdo.distribution-image@arch
  stage: test
  script:
    - mkdir -p _tarball_build
    - tar xf xorgproto-*.tar.gz -C _tarball_build
    - pushd _tarball_build/xorgproto-*
    - meson builddir
    - meson configure builddir
    - ninja -C builddir test
  needs:
    - autotools
  variables:
    GIT_STRATEGY: none

check evdev keysyms:
  extends:
    - .fdo.distribution-image@arch
  stage: test
  script:
    - ./scripts/keysym-generator.py --header=include/X11/XF86keysym.h verify

check formatting:
  extends:
    - .fdo.distribution-image@arch
  stage: check
  script:
    - black scripts/keysym-generator.py
    - git diff --exit-code || (echo "Please run Black against the Python script" && false)
  only:
    changes:
      - scripts/keysym-generator.py
